## ML_Project1

### Sberbank Russian Housing Market

https://www.kaggle.com/c/sberbank-russian-housing-market/overview

### Структура проекта

* data: данные для проекта
* notebooks: ноутбуки по проекту

### Инструкция по запуску

В папке Data необходимы .csv файлы 
* train.csv
* test.csv
* sample_submission.csv
